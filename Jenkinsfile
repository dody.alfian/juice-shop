pipeline {
  agent any
  environment {
    DASTARDLY_TARGET_URL='http://3.0.220.146/'
    IMAGE_WITH_TAG='public.ecr.aws/portswigger/dastardly:latest'
    JUNIT_TEST_RESULTS_FILE='dastardly-report.xml'
  }
  stages {
    stage('Semgrep For SAST Scanning') {
      steps {
        sh "docker run -v ${WORKSPACE}:/src --workdir /src returntocorp/semgrep-agent:v1 semgrep-agent --config p/ci"
      }
    }
    stage('Docker Image Build') {
      steps {
        script {
          def dockerImage = "juiceshop.${BUILD_ID}"
          sh "docker build -t ${dockerImage} ."
          sh "docker tag ${dockerImage} juiceshop:latest"
        }
      }
    }
    stage('Trivy For SCA Image Scan') {
      steps {
        sh 'trivy --severity CRITICAL juiceshop:latest'
      }
    }
    stage('Run Docker Image') {
      steps {
        sh 'docker run -d -p 80:80 juiceshop:latest'
      }
    }
    stage("Pull Dastardly Docker Image") {
      steps {
        sh 'docker pull ${IMAGE_WITH_TAG}'
      }
    }
    stage("Run Dastardly For DAST Scan") {
      steps {
        cleanWs()
        sh '''
          docker run --rm --user $(id -u) -v ${WORKSPACE}:${WORKSPACE}:rw \
          -e DASTARDLY_TARGET_URL=${DASTARDLY_TARGET_URL} \
          -e DASTARDLY_OUTPUT_FILE=${WORKSPACE}/${JUNIT_TEST_RESULTS_FILE} \
          ${IMAGE_WITH_TAG}
        '''
      }
    }
  }
  post {
    always {
      junit testResults: "${JUNIT_TEST_RESULTS_FILE}", skipPublishingChecks: true
    }
  }
}
